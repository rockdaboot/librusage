Librusage
=========

A small GNU/Linux tool to collect stats and timings of complex shell
commands and subcommands.

Building
--------

		git clone git@gitlab.com:rockdaboot/librusage.git
		cd librusage
		gcc -shared -fPIC librusage.c -o librusage.so -ldl

Usage example
-------------

		rm out.trace
		export LD_PRELOAD=~/src/librusage/librusage.so
		./configure
		unset LD_PRELOAD
		cat out.trace

		# Count of external commands, started by fork+execve
		grep execve out.trace|cut -d' ' -f5|sort|uniq -c|sort -n

		# Aggregate duration by command (output: invokations duration(ms) command
		grep ^1 out.trace|cut -d' ' -f4,7|grep ^[0-9]|awk '{ dur[$2]+=$1; n[$2]++; } END{ for (cmd in dur) print n[cmd], dur[cmd], cmd}'|sort -n

Format of output
----------------

Fields seperated by space.

1. timestamp, milliseconds since 1.1.1970
2. process id (pid)
3. parent process id (ppid)
4. real time of command in milliseconds (diff of constructor and destructor of this .so file)
5. user time of command in milliseconds (getrusage() in destructor)
6. system time of command in milliseconds (getrusage() in destructor)
7. command line taken from /proc/self/cmdline - watch out with multiline arguments

