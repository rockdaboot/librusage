/*
 * 2016 Written by Tim Rühsen
 *
 * # Compilation and usage example
 *   gcc -shared -fPIC librusage.c -o librusage.so -ldl
 *   rm out.trace
 *   export LD_PRELOAD=~/src/librusage/librusage.so
 *   ./configure
 *   unset LD_PRELOAD
 *   cat out.trace
 *
 * # Format of output
 *   1. timestamp, milliseconds since 1.1.1970
 *   2. process id (pid)
 *   3. parent process id (ppid)
 *   4. real time of command in milliseconds (diff of constructor and destructor of this .so file)
 *   5. user time of command in milliseconds (getrusage() in destructor)
 *   6. system time of command in milliseconds (getrusage() in destructor)
 *   7. command line taken from /proc/self/cmdline - watch out with multiline arguments
 *
 * # Count of external commands, started by fork+execve
 *   grep execve out.trace|cut -d' ' -f5|sort|uniq -c|sort -n
 *
 */

#define _GNU_SOURCE
#include <sched.h>
#include <dlfcn.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdarg.h>
#include <sys/procfs.h>
#include <sys/resource.h>
#include <sys/time.h>

#define PRINTF_FORMAT(a, b) __attribute__ ((format (printf, a, b)))

static void PRINTF_FORMAT(1,2) trace_printf(const char *fmt, ...)
{
	int fd;
	va_list args;

	if ((fd = open("out.trace", O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1)
		return;

	va_start(args, fmt);
	vdprintf(fd, fmt, args);
	va_end(args);

	close(fd);
}

static const char *get_cmdline(void)
{
	static char cmdline[2048];
	int fd, n;

	if ((fd = open("/proc/self/cmdline", O_RDONLY)) == -1)
		return NULL;

	if ((n = read(fd, cmdline, sizeof(cmdline) - 1)) < 0)
		n = 0;

	close(fd);

	cmdline[n] = 0;

	while (--n >= 0)
		if (!cmdline[n])
			cmdline[n] = ' ';

	return cmdline;
}

static struct timeval t1, t2;
static pid_t starter_pid;

static long long ms(struct timeval *t)
{
	return t->tv_sec * 1000LL + t->tv_usec / 1000;
}

static long long cur_ms(void)
{
	static struct timeval t;

	gettimeofday(&t, NULL);
	return ms(&t);
}

static long long dur_ms(struct timeval *t1, struct timeval *t2)
{
	return (t2->tv_sec - t1->tv_sec) * 1000LL + (t2->tv_usec - t1->tv_usec) / 1000;
}


static int (*libc_execve)(const char *filename, char *const argv[], char *const envp[]);
static pid_t (*libc_fork)(void);
static int (*libc_system)(const char *command);
static FILE *(*libc_popen)(const char *command, const char *type);

int execve(const char *filename, char *const argv[], char *const envp[])
{
	if (!strcmp(argv[0], "rm")) {
//		int argpos;
		int force = 0, recursive = 0;
		char *const *argp = argv + 1;

		for (; *argp; argp++) {
			if (**argp != '-' || !strcmp(*argp, "--"))
				break;

			if (!strcmp(*argp, "-r"))
				recursive = 1;
			else if (!strcmp(*argp, "-rf"))
				force = recursive = 1;
			else if (!strcmp(*argp, "-fr"))
				force = recursive = 1;
			else if (!strcmp(*argp, "-f"))
				force = 1;
		}

		if (!recursive && *argp) {
			char *const *save = argp;
			int wildcard = 0;

			for (; *argp; argp++) {
				if (strchr(*argp, '*'))
					{ wildcard = 1; break; }
			}
			if (!wildcard) {
				for (argp = save; *argp; argp++) {
//					fprintf(fp, "remove %s\n", *argp);
					unlink(*argp);
				}
				exit(0);
			}
		}
	}

	trace_printf("%lld %d %d execve %s\n", cur_ms(), getpid(), getppid(), filename);

	// gettimeofday(&t0, NULL);
	libc_execve(filename, argv, envp);

	return -1;
}

pid_t fork(void)
{
	trace_printf("%lld %d %d fork\n", cur_ms(), getpid(), getppid());

	return libc_fork();
}

FILE *popen(const char *command, const char *type)
{
	trace_printf("%lld %d %d popen %s %s\n", cur_ms(), getpid(), getppid(), command, type);

	return libc_popen(command, type);
}

pid_t vfork(void)
{
	trace_printf("%lld %d %d vfork\n", cur_ms(), getpid(), getppid());

	// using fork() cause vfork() needs some stack frame magic
	return libc_fork();
}

int system(const char *command)
{
	trace_printf("%lld %d %d system\n", cur_ms(), getpid(), getppid());

	return libc_system(command);
}

// Is not called on fork/clone, just on exec().
static void __attribute__((constructor)) start(void)
{
	gettimeofday(&t1, NULL);
	starter_pid = getpid();
	libc_execve = dlsym(RTLD_NEXT, "execve");
	libc_fork = dlsym(RTLD_NEXT, "fork");
	libc_popen = dlsym(RTLD_NEXT, "popen");
	libc_system = dlsym(RTLD_NEXT, "system");
}

// Is always called before process stops - so it doesn't pair up with
// the constructor.
static void __attribute__((destructor)) printstats(void)
{
	struct rusage u;
	long long duration, utime = 0, stime = 0;

	if (starter_pid != getpid())
		return; // I am a child without an exec (e.g. used for piping)

	gettimeofday(&t2, NULL);
	duration = dur_ms(&t1, &t2); // real duration in ms

	if (getrusage(RUSAGE_SELF, &u) == 0) {
		utime = ms(&u.ru_utime);
		stime = ms(&u.ru_stime);
	}

	trace_printf("%lld %d %d %lld %lld %lld %s\n",
		ms(&t2), getpid(), getppid(), duration, utime, stime, get_cmdline());
}
